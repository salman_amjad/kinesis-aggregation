/*
  Copyright 2014-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
  Licensed under the Amazon Software License (the 'License').
  You may not use this file except in compliance with the License.
  A copy of the License is located at
  http://aws.amazon.com/asl/
  or in the 'license' file accompanying this file. This file is distributed on an 'AS IS' BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
const crypto = require('crypto');
const common = require('./common');

const { AggregatedRecord } = common;
const internals = {};

/** asynchronous deaggregation interface */
internals.deaggregate = (kinesisRecord, computeChecksums) => {
  return new Promise(async (fulfill, reject) => {
    /* jshint -W069 */// suppress warnings about dot notation (use of
    // underscores in protobuf model)
    //
    // we receive the record data as a base64 encoded string
    const recordBuffer = Buffer.from(kinesisRecord.data, 'base64');

    // first 4 bytes are the kpl assigned magic number
    // https://github.com/awslabs/amazon-kinesis-producer/blob/master/aggregation-format.md
    const allRecords = [];
    if (recordBuffer.slice(0, 4).toString('hex') === common.magicNumber) {
      try {
        // decode the protobuf binary from byte offset 4 to length-16 (last
        // 16 are checksum)
        const protobufMessage = AggregatedRecord.decode(recordBuffer.slice(4, recordBuffer.length - 16));

        // extract the kinesis record checksum
        const recordChecksum = recordBuffer.slice(recordBuffer.length - 16, recordBuffer.length).toString('base64');

        if (computeChecksums === true) {
          // compute a checksum from the serialised protobuf message
          const md5 = crypto.createHash('md5');
          md5.update(recordBuffer.slice(4, recordBuffer.length - 16));
          const calculatedChecksum = md5.digest('base64');

          // validate that the checksum is correct for the transmitted
          // data
          if (calculatedChecksum !== recordChecksum) {
            if (common.debug) {
              console.log('Record Checksum: ', recordChecksum);
              console.log('Calculated Checksum: ', calculatedChecksum);
            }

            throw new Error('Invalid record checksum');
          }
        } else if (common.debug) {
          console.log('WARN: Record Checksum Verification turned off');
        }

        if (common.debug) console.log(`Found ${protobufMessage.records.length} KPL Encoded Messages`);

        // iterate over each User Record in order
        for (let i = 0; i < protobufMessage.records.length; i += 1) {
          try {
            const item = protobufMessage.records[i];

            // emit the per-record callback with the extracted partition
            // keys and sequence information
            allRecords.push({
              partitionKey: protobufMessage['partition_key_table'][item['partition_key_index']],
              explicitPartitionKey: protobufMessage['explicit_hash_key_table'][item['explicit_hash_key_index']],
              sequenceNumber: kinesisRecord.sequenceNumber,
              subSequenceNumber: i,
              data: item.data.toString('base64')
            });
          } catch (e) {
            reject(e);
          }
        }

        // finished processing the kinesis record
        fulfill(allRecords);
      } catch (e) {
        reject(e);
      }
    } else {
      // not a KPL encoded message - no biggie - emit the record with
      // the same interface as if it was. Customers can differentiate KPL
      // user records vs plain Kinesis Records on the basis of the
      // sub-sequence number
      if (common.debug) {
        console.log(`WARN: Non KPL Aggregated Message Processed for DeAggregation: ${kinesisRecord.partitionKey} - ${kinesisRecord.sequenceNumber}`);
      }

      allRecords.push({
        partitionKey: kinesisRecord.partitionKey,
        explicitPartitionKey: kinesisRecord.explicitPartitionKey,
        sequenceNumber: kinesisRecord.sequenceNumber,
        data: kinesisRecord.data
      });

      fulfill(allRecords);
    }
  });
};

module.exports = internals;
